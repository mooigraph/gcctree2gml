/* Output gml description file according to the tree dump file.

   Copyright (C) 2009, 2010 Mingjie Xing, mingjie.xing@gmail.com. 

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <unistd.h>
#include <error.h>
#include <obstack.h>

#include "gcctree2gml.h"

extern FILE *yyin;
extern int yyparse (void);
extern void set_yy_debug (void);

FILE *fin = NULL;
FILE *fout = NULL;
char *outname = NULL;

char *program_name = NULL;

#define obstack_chunk_alloc malloc
#define obstack_chunk_free free

struct obstack insn_obstack;

struct function *first_function;
struct function *last_function;
struct function *current_function;
struct basic_block *current_bb;

void
general_init (void)
{
  obstack_init (&insn_obstack);

  first_function = NULL;
  last_function = NULL;
  current_function = NULL;

  return;
}

int
main (int argc, char *argv[])
{
  program_name = argv[0];

  if (argc != 3)
    {
      printf ("Usage: %s gcc-tree-data-file output-file-name\n",
	      program_name);
      return (0);
    }

  if (handle_options (argc, argv) < 0)
    {
      return (0);
    }

  general_init ();

  yyin = fin;
  yyparse ();
  finish_previous_bb ();

  fine_tune_cfg ();

  cfg_to_gml ();

  return 0;
}
